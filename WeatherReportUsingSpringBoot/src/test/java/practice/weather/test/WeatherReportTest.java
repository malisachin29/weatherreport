package practice.weather.test;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import practice.weather.model.report.CityWeather;
import practice.weather.model.report.Forecast;
import practice.weather.model.report.ForecastLocation;
import practice.weather.model.report.HourlyForecasts;
import practice.weather.model.report.WeatherReport;
import practice.weather.service.WeatherService;
import practice.weather.service.WeatherServiceImpl;
import practice.weather.service.api.WeatherApiCall;



@RunWith(MockitoJUnitRunner.class)
public class WeatherReportTest {

	@Mock
	WeatherApiCall weatherApiCall;
	
	@InjectMocks
	WeatherService weatherService = new WeatherServiceImpl(weatherApiCall);

    @Test
    public void testGetWeatherReport() throws Exception {
    	
        WeatherReport weatherReport = new WeatherReport(); 
        
        Forecast forecast = new Forecast();    	
    	List<Forecast> forecasts = new ArrayList<Forecast>();
    	for (int i=0;i < 24 ;i++) {
    		forecast.setTemperature("10.10");
    		forecast.setLocalTime("0612312018");
    		forecasts.add(forecast);
		}
    	
    	ForecastLocation forecastLocation = new ForecastLocation();
    	forecastLocation.setForecast(forecasts);
    	forecastLocation.setCity("Addison");
    	
    	HourlyForecasts hourlyForecasts = new HourlyForecasts();
    	hourlyForecasts.setForecastLocation(forecastLocation);
    	
    	weatherReport.setHourlyForecasts(hourlyForecasts);
    	
    	when(weatherApiCall.getReport(75001)).thenReturn(weatherReport);
       	CityWeather cityWeather = weatherService.retrieveCoolestCityHourReport(75001);

    	Assert.assertThat(cityWeather, CoreMatchers.notNullValue());
        Assert.assertThat(cityWeather.getCityName(), CoreMatchers.equalTo("Addison"));
    }
    
}

package practice.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import practice.weather.model.report.CityWeather;
import practice.weather.service.WeatherService;
import practice.weather.service.WeatherServiceImpl;
import practice.weather.service.api.WeatherApiCall;
import practice.weather.service.api.WeatherApiCallImpl;

@SpringBootApplication
public class WeatherReportApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(WeatherReportApplication.class, args);
	}

	@Bean 
	public WeatherApiCall getWeatherApiCall(){ 
		return new WeatherApiCallImpl();
	}
	
	@Bean 
	public WeatherService getWeatherService(){ 
		return new WeatherServiceImpl(getWeatherApiCall());
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	 
	/**
	 * function : CommandLineRunner run method
	 * This is executed only once in application runtime after application is
	 * initialized.
	 */	
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		String input;
		try{
			System.out.println("Tomorrow predicted temperature");
			System.out.println("Enter the 5 digit Zip code of the city in USA");
			BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));

			while((input=br.readLine())!=null && !input.equals("exit") && !input.equals("quit")) {
				try {
					int zipCode = 75252;
					zipCode = Integer.parseInt(input);
					CityWeather cityWeather = getWeatherService().retrieveCoolestCityHourReport(zipCode);
					
					//Finds the coolest hour of tomorrow for the city of user filled zipcode
					System.out.println("Coolest HOUR in " + cityWeather.getCityName() + " tomorrow on " + cityWeather.getDate() + " would be at " + cityWeather.getTime());
					System.out.println("and the temperature would be " + cityWeather.getTemp() +  " F");
					
				}catch (NumberFormatException e) {
					System.out.println("Please enter a valid zip code");
				}

				System.out.println("Want to check temperature at another location,\nthen please Enter the 5 digit Zip code of the city in USA or enter exit to quit");

			}
			System.out.println(".....Program Exited.....");

		}catch (IOException e) {
			e.printStackTrace();
		}
	}

}

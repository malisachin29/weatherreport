package practice.weather.model.report;

public class WeatherReport {

	private HourlyForecasts hourlyForecasts;
	
	private String feedCreation;
	private String metric;
	
	
	public HourlyForecasts getHourlyForecasts() {
		return hourlyForecasts;
	}
	public void setHourlyForecasts(HourlyForecasts hourlyForecasts) {
		this.hourlyForecasts = hourlyForecasts;
	}
	public String getFeedCreation() {
		return feedCreation;
	}
	public void setFeedCreation(String feedCreation) {
		this.feedCreation = feedCreation;
	}
	public String getMetric() {
		return metric;
	}
	public void setMetric(String metric) {
		this.metric = metric;
	}
	
	
}

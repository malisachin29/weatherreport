package practice.weather.model.report;

public class HourlyForecasts {

	private ForecastLocation forecastLocation;

	public ForecastLocation getForecastLocation() {
		return forecastLocation;
	}

	public void setForecastLocation(ForecastLocation forecastLocation) {
		this.forecastLocation = forecastLocation;
	}
	
	
}

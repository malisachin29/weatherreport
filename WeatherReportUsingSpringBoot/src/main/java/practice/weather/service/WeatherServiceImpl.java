package practice.weather.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import practice.weather.model.report.CityWeather;
import practice.weather.model.report.Forecast;
import practice.weather.model.report.WeatherReport;
import practice.weather.service.api.WeatherApiCall;


public class WeatherServiceImpl implements WeatherService {
	private WeatherApiCall weatherApiCall;

	
	/*
	 * Constructor : WeatherServiceImpl.WeatherServiceImpl
	 * 
	 * input : WeatherApiCall 
	 * description : While testing we don't want to hit actual api for that reason
	 * we provided this constructor so that mock service can be injected while exexuting test
	 * case. And for actual use we provide WeatherApiCallImpl instance.
	 */
	public WeatherServiceImpl(WeatherApiCall weatherApiCall) {
		super();
		this.weatherApiCall = weatherApiCall;
	}
	
	
	
	/*
	 * function : WeatherServiceImpl.retrieveCoolestCityHourReport
	 * 
	 * input : zipcode 
	 * description : Call internal Weather api service to get weather report
	 * for provided zip code after getting report. We retrieve only required
	 * information. 
	 * output : Coolest of city and temperature at that time i.e.CityWeather
	 */
	public CityWeather retrieveCoolestCityHourReport(int zipCode) {
		WeatherReport weatherReport = weatherApiCall.getReport(zipCode);
		List<Double> temp = new ArrayList<Double>(); 
		List<String> dateTime = new ArrayList<String>();
		List<Forecast> forecasts = weatherReport.getHourlyForecasts().getForecastLocation().getForecast().subList(0, 24);
		for (Forecast forecast : forecasts) {
			temp.add(Double.parseDouble((forecast.getTemperature())));
			dateTime.add(forecast.getLocalTime());
		}

		int minIndex = temp.indexOf(Collections.min(temp));
		String time = dateTime.get(minIndex).substring(0,2);
		String date = dateTime.get(minIndex).substring(2, dateTime.get(minIndex).length());
		date = date.substring(0,2) + "-" + date.substring(2,4) + "-" + date.substring(4,8);
		String meridiem = "am";
		//Display time in meridiem
		if(Integer.parseInt(time) > 11){
			if(time!="12")
				time = Integer.toString((Integer.parseInt(time) - 12));
			meridiem = "pm";
		}else{
			if(time=="00")
				time = "12";
		}	

		return new CityWeather(weatherReport.getHourlyForecasts().getForecastLocation().getCity(), date, time + " " + meridiem, temp.get(minIndex));	
	}


}
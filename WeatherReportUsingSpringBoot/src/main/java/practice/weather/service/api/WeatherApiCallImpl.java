package practice.weather.service.api;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import practice.weather.model.report.WeatherReport;

public class WeatherApiCallImpl implements WeatherApiCall{

	@Autowired
	RestTemplate restTemplate;
	
	
	/*
	 * function : WeatherApiCallImpl.getReport
	 * 
	 * input : zipcode 
	 * description : Uses Spring restTemplate to get weather report. By taking information of weather api such as 
	 * (app.url , app.id , app.code) from property file and provided zip code.
	 * The restTemplate convert response into WeatherReport model. 
	 * output : WeatherReport
	 */
	public WeatherReport getReport(int zipCode){
		try {
				// TODO Auto-generated method stub
				Properties prop = new Properties();
				//load a properties file from class path, inside static method
				prop.load(WeatherApiCallImpl.class.getClassLoader().getResourceAsStream("config.properties"));
		
				String apiUrl = prop.getProperty("app.url") + "?app_id="+prop.getProperty("app.id")+"&app_code="+ 
						prop.getProperty("app.code")+ "&product=forecast_hourly&metric=false&zipcode=" + 
						zipCode;
				
				
				return restTemplate.getForObject(apiUrl, WeatherReport.class);
			}catch (IOException e) {
				System.out.println("City not found, please enter a valid Zip Code");
			}
		return null;
	}
}

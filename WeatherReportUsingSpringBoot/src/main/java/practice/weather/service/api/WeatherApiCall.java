package practice.weather.service.api;

import practice.weather.model.report.WeatherReport;


public interface WeatherApiCall {
	public WeatherReport getReport(int zipCode);	
}

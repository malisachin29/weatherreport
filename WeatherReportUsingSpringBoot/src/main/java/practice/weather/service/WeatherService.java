package practice.weather.service;

import practice.weather.model.report.CityWeather;

@FunctionalInterface
public interface WeatherService {
	public CityWeather retrieveCoolestCityHourReport(int zipCode);	
}
